package com.yqh.wechat.entity;

import java.util.Date;

public class RedPacketSendRecord {
	
	private Long sendRecordId;

	private Long userId;

	private Long cashPackageId;

	private Long siteId;

	private String openId;

	// 父标记
	private String parentOpenId;

	//来源媒体id
	private Integer mediaId;

	private String orderId;

	//红包类型
	private Integer redType;

	// 留电红包使用
	private String phone;

	//领取时间
	private Date date;

	private String nickName;
	
	private Integer sex;

	// 地区信息
	private String area;
	
	private String headImgUrl;
	
	private Integer amount;
	
	private String ext;
	
	private Integer status;

	private String reason;
	
	private Date createTime;
	
	private String url;

	public Long getSendRecordId() {
		return sendRecordId;
	}

	public void setSendRecordId(Long sendRecordId) {
		this.sendRecordId = sendRecordId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getCashPackageId() {
		return cashPackageId;
	}

	public void setCashPackageId(Long cashPackageId) {
		this.cashPackageId = cashPackageId;
	}

	public Long getSiteId() {
		return siteId;
	}

	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getParentOpenId() {
		return parentOpenId;
	}

	public void setParentOpenId(String parentOpenId) {
		this.parentOpenId = parentOpenId;
	}

	public Integer getMediaId() {
		return mediaId;
	}

	public void setMediaId(Integer mediaId) {
		this.mediaId = mediaId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Integer getRedType() {
		return redType;
	}

	public void setRedType(Integer redType) {
		this.redType = redType;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getHeadImgUrl() {
		return headImgUrl;
	}

	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	
	
    
    
    

}