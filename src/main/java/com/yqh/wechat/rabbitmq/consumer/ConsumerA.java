package com.yqh.wechat.rabbitmq.consumer;

import java.io.IOException;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.rabbitmq.client.Channel;
import com.yqh.wechat.rabbitmq.config.RabbitMqConfig;

/**
 * 消息消费者A
 * @author Yang.Qinghui
 */
@Component
@RabbitListener(queues = {RabbitMqConfig.QUEUE_A,RabbitMqConfig.QUEUE_B})
public class ConsumerA {

	@RabbitHandler
    public void process(String context, Channel channel, Message message) throws IOException {
		System.out.println("[ ConsumerA process ] >>> " + context);
        try {
            //告诉服务器收到这条消息 已经被我消费了 可以在队列删掉 这样以后就不会再发了 否则消息服务器以为这条消息没处理掉 后续还会在发
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
            System.out.println("[ ConsumerA process ] success");
        } catch (IOException e) {
            e.printStackTrace();
            //丢弃这条消息
            //channel.basicNack(message.getMessageProperties().getDeliveryTag(), false,false);
            System.out.println("[ ConsumerA process ] fail");
        }
 
    }

	
}
