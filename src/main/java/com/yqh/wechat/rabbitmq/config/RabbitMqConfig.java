package com.yqh.wechat.rabbitmq.config;


import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.yqh.wechat.rabbitmq.mqcallback.MsgSendConfirmCallBack;
import com.yqh.wechat.rabbitmq.mqcallback.MsgSendReturnCallback;

/**
 * RabbitMq配置
 * RabbitMq介绍：https://www.cnblogs.com/dwlsxj/p/RabbitMQ.html
 * 代码来源：https://blog.csdn.net/zhuzhezhuzhe1/article/details/80454956
 * @author Yang.Qinghui
 */
@Configuration
public class RabbitMqConfig {
	
	// 消息交换机的名字
    public static final String EXCHANGE = "exchange";
    
    public static final String QUEUE_A = "queue_A";
    // 队列key1
    public static final String ROUTINGKEY_A = "routingkey_A";
    
    public static final String QUEUE_B = "queue_B";
    // 队列key2
    public static final String ROUTINGKEY_B = "routingkey_B";
 
    @Autowired
    private QueueConfig queueConfig;
    
    @Autowired
    private ExchangeConfig exchangeConfig;
    
 
    /**
     * 连接工厂
     */
    @Autowired
    private ConnectionFactory connectionFactory;
 
    /**
     * 将消息队列A和交换机进行绑定
     */
    @Bean
    public Binding bindingA() {
        return BindingBuilder.bind(queueConfig.queueA()).to(exchangeConfig.directExchange()).with(RabbitMqConfig.ROUTINGKEY_A);
    }
 
    /**
     * 将消息队列B和交换机进行绑定
     */
    @Bean
    public Binding bindingB() {
        return BindingBuilder.bind(queueConfig.queueB()).to(exchangeConfig.directExchange()).with(RabbitMqConfig.ROUTINGKEY_B);
    }
 
// ---------------------------------------------------- 下面在springboot里没有必要配置------------------------------------------------    
    
    
    /**
     * queue listener 观察 监听模式
     * 当有消息到达时会通知监听在对应的队列上的监听对象
     * @return
     */
//    @Bean		// 注释，使其不生效
    public SimpleMessageListenerContainer simpleMessageListenerContainer_one(){
    	
    	//加载处理消息A的队列
        SimpleMessageListenerContainer simpleMessageListenerContainer = new SimpleMessageListenerContainer(connectionFactory);
        
        //设置接收多个队列里面的消息，这里设置接收队列A
        //假如想一个消费者处理多个队列里面的信息可以如下设置：
        //container.setQueues(queueA(),queueB(),queueC());
        simpleMessageListenerContainer.addQueues(queueConfig.queueA());
        simpleMessageListenerContainer.setExposeListenerChannel(true);
        //设置最大的并发的消费者数量
        simpleMessageListenerContainer.setMaxConcurrentConsumers(2);
        //最小的并发消费者的数量
        simpleMessageListenerContainer.setConcurrentConsumers(1);
        //设置确认模式手工确认
        simpleMessageListenerContainer.setAcknowledgeMode(AcknowledgeMode.MANUAL);
        simpleMessageListenerContainer.setMessageListener(new ChannelAwareMessageListener() {
            public void onMessage(Message message, com.rabbitmq.client.Channel channel) throws Exception {
                byte[] body = message.getBody();
                System.out.println("消费端接收到消息 : " + new String(body));
                channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
            }
        });
        return simpleMessageListenerContainer;
    }
 
    /**
     * 定义rabbit template用于数据的接收和发送
     * @return
     */
//    @Bean
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        /**
         * 若使用confirm-callback或return-callback，
         * 必须要配置publisherConfirms或publisherReturns为true
         * 每个rabbitTemplate只能有一个confirm-callback和return-callback
         */
        template.setConfirmCallback(msgSendConfirmCallBack());
//         template.setReturnCallback(msgSendReturnCallback());
        /**
         * 使用return-callback时必须设置mandatory为true，或者在配置中设置mandatory-expression的值为true，
         * 可针对每次请求的消息去确定’mandatory’的boolean值，
         * 只能在提供’return -callback’时使用，与mandatory互斥
         */
//         template.setMandatory(true);
        return template;
    }
 
    /**
     * 消息确认机制
     * Confirms给客户端一种轻量级的方式，能够跟踪哪些消息被broker处理，
     * 哪些可能因为broker宕掉或者网络失败的情况而重新发布。
     * 确认并且保证消息被送达，提供了两种方式：发布确认和事务。(两者不可同时使用)
     * 在channel为事务时，不可引入确认模式；同样channel为确认模式下，不可使用事务。
     * @return
     */
    @Bean
    public MsgSendConfirmCallBack msgSendConfirmCallBack(){
        return new MsgSendConfirmCallBack();
    }


    @Bean
    public MsgSendReturnCallback msgSendReturnCallback(){
        return new MsgSendReturnCallback();
    }
    
}
