package com.yqh.wechat.util;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.SortedMap;

import org.apache.log4j.Logger;

public class WechatUtil {
	
	private static SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMddHHmmss");
	
	private static Logger log= Logger.getLogger(WechatUtil.class); 
	
	
	 /**  
     * @Description：sign签名  
     * @param characterEncoding  
     *            编码格式  
     * @param parameters  
     *            请求参数  
     * @return  
     */    
    public static String createSign(String characterEncoding, SortedMap<Object, Object> packageParams, String API_KEY) {    
        StringBuffer sb = new StringBuffer();    
        Set es = packageParams.entrySet();    
        Iterator it = es.iterator();    
        while (it.hasNext()) {    
            Map.Entry entry = (Map.Entry) it.next();    
            String k = (String) entry.getKey();    
            String v = (String) entry.getValue();    
            if (null != v && !"".equals(v) && !"sign".equals(k) && !"key".equals(k)) {    
                sb.append(k + "=" + v + "&");    
            }    
        }    
        sb.append("key=" + API_KEY);    
        String sign = MD5Util.MD5Encode(sb.toString(), characterEncoding).toUpperCase();    
        return sign;    
    }    
	
	
	/**  
     * 是否签名正确,规则是:按参数名称a-z排序,遇到空值的参数不参加签名。  
     * @return boolean  
     */    
    public static boolean isTenpaySign(String characterEncoding, SortedMap<Object, Object> packageParams, String API_KEY) {    
        StringBuffer sb = new StringBuffer();    
        Set es = packageParams.entrySet();    
        Iterator it = es.iterator();    
        while(it.hasNext()) {    
            Map.Entry entry = (Map.Entry)it.next();    
            String k = (String)entry.getKey();    
            String v = (String)entry.getValue();    
            if(!"sign".equals(k) && null != v && !"".equals(v)) {    
                sb.append(k + "=" + v + "&");    
            }    
        }    
        sb.append("key=" + API_KEY);    

        //算出摘要    
        String mysign = MD5Util.MD5Encode(sb.toString(), characterEncoding).toLowerCase();    
        String tenpaySign = ((String)packageParams.get("sign")).toLowerCase();    

        return tenpaySign.equals(mysign);    
    }    


    /**  
     * @Description：将请求参数转换为xml格式的string  
     * @param parameters  
     *            请求参数  
     * @return  
     */    
    public static String getRequestXml(SortedMap<Object, Object> parameters) {    
        StringBuffer sb = new StringBuffer();    
        sb.append("<xml>");    
        Set es = parameters.entrySet();    
        Iterator it = es.iterator();    
        while (it.hasNext()) {    
            Map.Entry entry = (Map.Entry) it.next();    
            String k = (String) entry.getKey();    
            String v = (String) entry.getValue();    
            if ("attach".equalsIgnoreCase(k) || "body".equalsIgnoreCase(k) || "sign".equalsIgnoreCase(k)) {    
                sb.append("<" + k + ">" + "<![CDATA[" + v + "]]></" + k + ">");    
            } else {    
                sb.append("<" + k + ">" + v + "</" + k + ">");    
            }    
        }    
        sb.append("</xml>");    
        return sb.toString();    
    }    

    /**  
     * 取出一个指定长度大小的随机正整数.  
     * @param length  
     *            int 设定所取出随机数的长度。length小于11  
     * @return int 返回生成的随机数。  
     */    
    public static int buildRandom(int length) {    
        int num = 1;    
        double random = Math.random();    
        if (random < 0.1) {    
            random = random + 0.1;    
        }    
        for (int i = 0; i < length; i++) {    
            num = num * 10;    
        }    
        return (int) ((random * num));    
    }    

    /**  
     * 获取当前时间 yyyyMMddHHmmss  
     * @return String  
     */    
    public static String getCurrTime() {    
        return SDF.format(new Date());    
    }  
    
    
    /**
     * 获取一定长度的随机字符串
     * @param length 指定字符串长度
     * @return 一定长度的字符串
     */
    public static String getRandomStringByLength(int length) {
        String base = "abcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }
    
    /** 
     * 获取本机IP地址 
     * @return 
     */  
    public static String localIp(){  
        String ip = null;  
        Enumeration allNetInterfaces;  
        try {  
            allNetInterfaces = NetworkInterface.getNetworkInterfaces();              
            while (allNetInterfaces.hasMoreElements()) {  
                NetworkInterface netInterface = (NetworkInterface) allNetInterfaces.nextElement();  
                List<InterfaceAddress> InterfaceAddress = netInterface.getInterfaceAddresses();  
                for (InterfaceAddress add : InterfaceAddress) {  
                    InetAddress Ip = add.getAddress();  
                    if (Ip != null && Ip instanceof Inet4Address) {  
                        ip = Ip.getHostAddress();  
                    }  
                }  
            }  
        } catch (SocketException e) {  
        	log.warn("获取本机Ip失败:异常信息:"+e.getMessage());  
        }  
        return ip;  
    }  

}
