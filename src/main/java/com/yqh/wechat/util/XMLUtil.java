package com.yqh.wechat.util;

import java.io.ByteArrayInputStream;  
import java.io.IOException;  
import java.io.InputStream;  
import java.util.HashMap;  
import java.util.Iterator;  
import java.util.List;  
import java.util.Map;  

import org.apache.commons.lang3.StringUtils;
import org.jdom.Document;  
import org.jdom.Element;  
import org.jdom.JDOMException;  
import org.jdom.input.SAXBuilder;  


public class XMLUtil {  

	/**  
     * @param strxml  
     * @return  
     * @throws JDOMException  
     * @throws IOException  
     */    
	@SuppressWarnings("rawtypes")
	public static Map<String,String> doXMLParse(String strxml){    
		
		if(StringUtils.isBlank(strxml))	return null;
		
        strxml = strxml.replaceFirst("encoding=\".*\"", "encoding=\"UTF-8\"");    
        if(StringUtils.isBlank(strxml))	return null;    
        
        Map<String,String> map = new HashMap<>();    
        
        InputStream in = null;
        try {
			in = new ByteArrayInputStream(strxml.getBytes("UTF-8"));    
			SAXBuilder builder = new SAXBuilder();    
			Document doc = builder.build(in);    
			Element root = doc.getRootElement();    
			List list = root.getChildren();    
			Iterator it = list.iterator();    
			while(it.hasNext()) {    
			    Element e = (Element) it.next();    
			    String k = e.getName();    
			    String v = "";    
			    List children = e.getChildren();    
			    if(children.isEmpty()) {    
			        v = e.getTextNormalize();    
			    } else {    
			        v = XMLUtil.getChildrenText(children);    
			    }    

			    map.put(k, v);    
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				//关闭流    
				if(in != null)	in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}    
		}

        return map;    
    }    

    /**  
     * 获取子结点的xml  
     * @param children  
     * @return String  
     */    
    @SuppressWarnings("rawtypes")
	public static String getChildrenText(List children) {    
        StringBuffer sb = new StringBuffer();    
        if(!children.isEmpty()) {    
            Iterator it = children.iterator();    
            while(it.hasNext()) {    
                Element e = (Element) it.next();    
                String name = e.getName();    
                String value = e.getTextNormalize();    
                List list = e.getChildren();    
                sb.append("<" + name + ">");    
                if(!list.isEmpty()) {    
                    sb.append(XMLUtil.getChildrenText(list));    
                }    
                sb.append(value);    
                sb.append("</" + name + ">");    
            }    
        }    

        return sb.toString();    
    }    
}  

